<?php

include_once '/var/www/html/include/config.security.php';
include_once '/var/www/html/include/db.php';
include_once '/var/www/html/include/file_functions.php';
include_once '/var/www/html/include/general.php';
include_once '/var/www/html/include/collections_functions.php';
include_once '/var/www/html/include/definitions.php';

$admin_username = getenv('RS_ADMIN_USERNAME');
$admin_password = getenv('RS_ADMIN_PASSWORD');
$admin_email = getenv('RS_ADMIN_EMAIL');
$admin_fullname = getenv('RS_ADMIN_FULLNAME');

$password_hash = hash('sha256', md5('RS' . $admin_username . $admin_password));

// Existing user?
$user_count = sql_value("SELECT count(*) value FROM user WHERE username = '" . escape_check($admin_username) . "'", 0);
if(0 == $user_count) {
    // No existing matching user. Insert.
    // Note: First user should always be part of Super Admin, hence user group is set to 3
    $sql_query = "INSERT INTO user(username, password, fullname, email, usergroup) VALUES('" . escape_check($admin_username) . "', '" . $password_hash . "', '" . escape_check($admin_fullname) . "', '" . escape_check($admin_email) . "', 3)";

    // Perform the insert / update
    sql_query($sql_query);
}
?>
