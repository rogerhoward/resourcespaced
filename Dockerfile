FROM php:7.2-apache

# Get RS_VERSION variable
ARG RS_VERSION

# Create logs directory
RUN mkdir -p /logs

# Install dependencies
RUN apt-get update && apt-get install -y \
        libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev libfreetype6-dev \
        imagemagick exiftool ffmpeg antiword poppler-utils ghostscript \
        p7zip-full \
        zlib1g-dev libzip-dev \
        libicu-dev g++ \
        git git-svn \
        nano \
    && rm -rf /var/lib/apt/lists/*
	# && docker-php-ext-configure gd \
	# && docker-php-ext-install -j$(nproc) gd mysqli intl zip exif \

RUN docker-php-ext-install mysqli intl zip exif mbstring

RUN docker-php-ext-configure gd \
    --with-webp-dir=/usr/include/ \
    --with-freetype-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-xpm-dir=/usr/include/

RUN docker-php-ext-install gd

# Purge install artifacts
RUN apt-get purge -y \
    build-essential \
    && apt-get autoremove -y

# Customize Apache startup script to include init.php
COPY apache2-foreground /usr/local/bin/apache2-foreground
RUN chmod +x /usr/local/bin/apache2-foreground

# Setup php.ini
COPY php.ini-production /usr/local/etc/php/php.ini

# Create filestore and grant www-data ownership
RUN mkdir -p /filestore && chown -R www-data:www-data /filestore

# Change to user www-data and finish up
USER www-data

# Download Resourcespace release based on RS_VERSION
RUN echo "downloading Resourcespace ${RS_VERSION}"
WORKDIR /var/www/html
RUN git svn clone --quiet http://svn.resourcespace.com/svn/rs/releases/${RS_VERSION} ./
RUN rm -fR ./.git

# Create filestore
RUN mkdir -p /var/www/html/filestore

# Use config.php as default config
COPY config.php /var/www/html/include/config.php

# Create resourcespace.config with various RS artifacts
COPY config/ /resourcespace.config

USER root
RUN chown -R www-data:www-data /var/www/html/
